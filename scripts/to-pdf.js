#!/usr/bin/env node

const path = require("path");
const puppeteer = require("puppeteer");

// https://www.npmjs.com/package/file-url
function fileUrl(str) {
  if (typeof str !== "string") {
    throw new Error("Expected a string");
  }

  var pathName = path.resolve(str).replace(/\\/g, "/");

  // Windows drive letter must be prefixed with a slash
  if (pathName[0] !== "/") {
    pathName = "/" + pathName;
  }

  return encodeURI("file://" + pathName);
}

// https://www.npmjs.com/package/puppeteer-cli
const toPdf = async (url, destination) => {
  const browser = await puppeteer.launch({
    defaultViewport: { width: 1280, height: 1024 }
  });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });
  await page.pdf({ path: destination, format: "A4" });

  await browser.close();
};

if (process.argv.length === 4) {
  console.log("file name: ", process.argv[2]);
  const url = fileUrl(process.argv[2]);
  console.log("Source url: ", url);
  toPdf(url, process.argv[3]);
} else {
  console.log("Error: to-pdf requires exactly two arguments.");
}
