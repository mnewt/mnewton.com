const path = require("path");

module.exports = {
  siteMetadata: {
    title: `www.mnewton.com`,
    author: `Matthew Sojourner Newton`,
    description: `IT Consultant, &c.`,
    siteUrl: `https://www.mnewton.com/`,
    social: {
      github: `mnewt`,
    },
    menu: [
      [`Blog`, `https://blog.mnewton.com/`],
      // [`Projects`, `/projects/`],
      // [`Consulting`, `/consulting/`],
    ],
  },
  plugins: [
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          require(`postcss-import`),
          require(`postcss-modules-values`),
          require(`postcss-preset-env`)({
            stage: 0,
            features: {
              "color-mod-function": { unresolved: "warn" },
            },
          }),
          require(`postcss-nested`),
          require(`postcss-normalize`),
          require(`postcss-flexbugs-fixes`),
          require(`lost`),
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: path.join(__dirname, `src`, `pages`),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: path.join(__dirname, `src`, `data`),
      },
    },
    `gatsby-transformer-yaml`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Matthew Sojourner Newton`,
        short_name: `mnewt`,
        start_url: `/`,
        background_color: `#fcfcfc`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
  ],
};
