module.exports = {
  siteMetadata: {
    title: `Blog of Matthew Sojourner Newton`,
    author: `Matthew Sojourner Newton`,
    description: `IT Consultant, &c.`,
    siteUrl: `https://blog.mnewton.com/`,
    social: [[`github`, `mnewt`]],
    menu: [
      [`Blog`, `/`],
      // [`Projects`, `https://www.mnewton.com/projects/`],
      // [`Consulting`, `https://www.mnewton.com/consulting/`],
    ],
    keywords: [`blog`, `consulting`, `it`],
  },
  plugins: [
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [
          require(`postcss-import`),
          require(`postcss-modules-values`),
          require(`postcss-preset-env`)({
            stage: 0,
            features: {
              "color-mod-function": { unresolved: "warn" },
            },
          }),
          require(`postcss-nested`),
          require(`postcss-normalize`),
          require(`postcss-flexbugs-fixes`),
          require(`lost`),
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/articles`,
        name: `blog`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1000,
              wrapperStyle: `width: 100%;`,
              withWebp: true,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
        {
          site {
            siteMetadata {
              title
              description
              siteUrl
              site_url: siteUrl
            }
          }
        }
      `,
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) =>
              allMarkdownRemark.edges.map((edge) =>
                Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  custom_elements: [{ "content:encoded": edge.node.html }],
                }),
              ),
            query: `
            {
              allMarkdownRemark(limit: 1000, sort: {frontmatter: {date: DESC}}) {
                edges {
                  node {
                    excerpt
                    html
                    fields { slug }
                    frontmatter {
                      title
                      date
                    }
                  }
                }
              }
            }
          `,
            output: "/feed.xml",
            title: "Blog of Matthew Sojourner Newton",
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Matthew Sojourner Newton`,
        short_name: `mnewt`,
        start_url: `/`,
        background_color: `#fcfcfc`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-react-helmet`,
  ],
};
