---
title: Give The Client What They Need
date: 2019-04-04
---

## Ask for / Want / Need

There's an idea that's always been central to my consulting approach. In my mind it's the definition of consulting. It's the notion that I want to give the customer not simply what they ask for, but what they (1) need and (2) want.

Let me be clear. This is in no way to disrespect the client or their wishes. Rather, the idea is to recognize that the consultant's role is very different from the client's. The client has their whole world to think about. The consultant has a limited scope of engagement and with that comes focus. Focus is one of the most powerful abilities of the mind. 
