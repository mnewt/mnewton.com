---
title: Computer Security
author: Matthew Newton
date: 2019-07-01
---

See [On Security and Privacy](/on-security-and-privacy)

## Goal

Security is a big topic so we need to circumscribe our goal for this article. My hope is that you can sit down at your computer and go through this whole article in 15 minutes or less. When you are done, you will have done much more than most to secure all the personal information that courses through your computer. More than that--you will feel happy and tranquil.

## Operating System

### Password

Use a password with at least 8 characters to log in to your computer.[^1]

#### Mac

- `System Preferences` > `Security & Privacy`
  - `General` > `Set/Change Password`
  - `Require password *immediately*`

#### Windows

- TBD

### Install Updates

Configure your computer to install updates automatically and ensure that it does so regularly.

#### Mac

Steps

#### Windows

Steps

### Browser

[^1]: Do not use whole words or names, even with substituted letters (e.g. `p@$$w0rd` is little better than password but `psstwird` would be much better because it is not made up of one or more English words). Draw your passwords from at least uppercase letters, lowercase letters, and symbols. Do remember though to keep it easy for you to remember and and type, since you will type if often. Do not worry about changing it. Spend a few minutes and think up a good password. Then stick with it unless you have reason to believe it has been compromised.
