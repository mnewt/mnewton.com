---
title: "Computer Security and Privacy"
date: 2018-03-08T13:49:41-08:00
aliases: 
  - /browser/
---

## Basics

### Software Updates

- Keep your operating system and applications up to date. Whenever *Windows Update* or the *App Store* prompts you to update, do so.
- Use a password to log in
  - Mac
    - `System Preferences` -> `Security & Privacy` -> `Change Password`
    - `System Preferences` -> `Security & Privacy` -> `Require password 5 seconds after sleep begins`
- Encrypt your data
  - Mac
  - `System Preferences` -> `Security & Privacy` -> `FileVault` -> `Turn On FileVault`
- Enable the network firewall
  - Mac
  - `System Preferences` -> `Security & Privacy` -> `Firewall` -> `Turn On Firewall`
- Back up your data
  - Use Dropbox, iCloud, or a similar service and save all files to your synchronized folders. Assume you will lose everything that is not saved to those folders.

## Applications

- Password Manager
  
  - 1Password is my favorite because of its security track record and its user experience.
  - Bitwarden is good, and free/cheaper

## Services

- VPN
  - Private Internet Access
  - NordVPN

## Firefox

Firefox is now my recommended browser. Its performance has eclipsed other browsers and Google has started doing some things with Chrome that are increasingly hostile to users. 

### Essential Plugins

- uBlock Origin
- HTTPS Everywhere

### Recommended Plugins

- Cookie Autodelete
- Referer Control

## Chrome

If you prefer Google Chrome, you can still get a good experience with the following plugins.

