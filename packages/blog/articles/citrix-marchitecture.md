---
date: 2013-04-10T00:00:00Z
title: Citrix Marchitecture
---

I love Citrix and they make some great stuff. But let's be honest. It's no secret that some of their products are duct taped together and that their products' names change at least as often as they are released. But right now I'm being forced to get my CCEE certification to meet a client's requirements and I'm reminded just how bad it is.

After years of working with Citrix, I'm pretty sure this is how the product development process goes:

Exec to Engineer: What's the minimum we need to do to squeak this new sexy feature out?

Engineer: Well, it's complicated, but I prepared a 64-point, statistically rigorous, peer-reviewed high level proposal which I will now present to you...

Exec: That's whatever you were talking about for you. I'll give you 9.2% of that because I'm using the rest on our upcoming Six Sigma Yacht Polo retreat. You are not invited by the way.

...later...

Exec to Marketing: Engineering has sent me some long files that I can only guess have lots of long words in them. Take the chapter titles and make PowerPoints out of them. Whatever you do, don't read past the chapter titles or you could end up just like those engineers!

...later...

Press Release: Citrix has just released an awesome product that is so revolutionary it's never even heard of any other Citrix products!

...back in Santa Clara...

Engineer to Exec: That sounds great but you know we don't make anything like that, right? 

Exec: Sure we do, you said so yourself in some document or whatever. 

Engineer: Did you read it?

Exec: Read what?

Engineer: The proposal, the report, the press release... 

Exec: I have people for that. You better ship that product that Marketing just announced!

Engineer: But this press release doesn't make any sense. At a quick read it sounds great, but upon closer inspection, it's just not in any known language. All of these words are more or less technical terms, but it's really just one malpropism glued to another. You can't just re-invent existing technical terms to suit your marketing.

Exec: Nonsense. People will read into it whatever they want, then money. You just have to rewrite the manual.

...later...

Engineer: I have written the admin manual equivalent of The Jabberwocky. The serpent has eaten its own tail. I just hope no one from Professional Services gets my number.


Which leads back to me and studying for my exam:
---

Citrix to me: Please study this copy of The Jabberwocky. You will be tested on your ability to assimilate and regurgitate its diction.
