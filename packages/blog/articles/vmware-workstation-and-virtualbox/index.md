---
date: 2016-06-21T00:00:00Z
title: Virtualbox and VMware Workstation bridged networking on Windows
---

I use VMware Workstation for most of my virtual machines on my Windows development machine. However, I also like to use [vagrant](https://www.vagrantup.com/) to manage development projects. While I could pay HashiCorp $80 to get vagrant to work with VMware Workstation, I don't see the value. Vagrant works perfectly out of the box with VirtualBox.

#### So, VMware Workstation and VirtualBox need to cohabitate on the same machine

Mostly, they work fine together. But I found out the hard way that as soon as I installed VirtualBox, all of my VMware Workstation machines using bridged networking became disconnected. After some digging, I found the reason.

VMware Workstation automatically chooses the adapter to use for bridged networking. Once VirtualBox is installed, it apparently chose the 'VirtualBox Host-Only Ethernet Adapter.' Once you know this, it's easy to fix.

## The fix

![Virtual Network Editor](./vmnet-editor.png)

1. From VMware Workstation choose `Edit -> Virtual Network Editor...`
2. At the bottom of the dialog, click `Change Settings` to elevate to administrator credentials
3. From the list at the top, choose the `Bridged` type virtual network
4. Under `VMnet Information -> Bridged` click `Automatic Settings`
5. Uncheck `VirtualBox Host-Only Ethernet Adapter` and click OK, OK
